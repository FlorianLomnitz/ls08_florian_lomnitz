package Auftrag3;

import java.io.*;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.json.JsonWriter;

/* Auftrag 3) Erstellen Sie notwendige Klassen und erstellen Sie folgende buchhandlung3.json:
{
"name": "OSZIMT Buchhandlung",
"tel": "030-225027-800",
"fax": "030-225027-809",
"adresse": {
"strasse": "Haarlemer Strasse",
"hausnummer": "23-27",
"plz": "12359",
"ort": "Berlin"
}
}

*/

public class WriteBookstoreDataJason {

	public static void main(String[] args) {

		JsonObjectBuilder bookBuilder = Json.createObjectBuilder();
		bookBuilder.add("name", "OSZ IMT Buchhandlung");
		bookBuilder.add("tel", "030-225027-800");
		bookBuilder.add("fax", "030-225027-809");

		JsonObjectBuilder adresselisteBuilder = Json.createObjectBuilder();
		adresselisteBuilder.add("strasse", "Haarlemer Strasse");
		adresselisteBuilder.add("hausnummer", "23-27");
		adresselisteBuilder.add("plz", "12359");
		adresselisteBuilder.add("ort", "Berlin");
		bookBuilder.add("adresse", adresselisteBuilder);

		JsonObject jo = bookBuilder.build();

		try {
			FileWriter fw = new FileWriter("book.json");
			JsonWriter jw = Json.createWriter(fw);
			jw.write(jo);
			fw.close();
			jw.close();

		} catch (Exception ex) {
			ex.printStackTrace();

		}

	}

}

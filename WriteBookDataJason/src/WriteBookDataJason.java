
/*
 {
 	"titel" : "XQuery Kick Start",
 	

 }

*/
import java.io.*;
import javax.json.*;
import javax.json.spi.*;

public class WriteBookDataJason {

	public static void main(String[] args) {

		JsonObjectBuilder bookBuilder = Json.createObjectBuilder();
		bookBuilder.add("titel", "XQuery Kick Start");

		JsonObjectBuilder verlagBuilder = Json.createObjectBuilder();
		verlagBuilder.add("name", "Sams");
		verlagBuilder.add("ort", "indianapolis");
		bookBuilder.add("verlag", verlagBuilder);

		JsonArrayBuilder autorenBuilder = Json.createArrayBuilder();
		autorenBuilder.add("James McGovern");
		autorenBuilder.add("Per Bothner");
		autorenBuilder.add("Kurt Cagle");
		autorenBuilder.add("James Linn");
		bookBuilder.add("autoren", autorenBuilder);

		JsonObject jo = bookBuilder.build();

		try {
			FileWriter fw = new FileWriter("book.json");
			JsonWriter jw = Json.createWriter(fw);
			jw.write(jo);
			fw.close();
			jw.close();
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

}
